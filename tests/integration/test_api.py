from lambda_function import lambda_handler


def test_ping():
    # Fix this with fixture
    resp = lambda_handler({'rawPath': '/ping'}, None)
    assert resp == '{"success": true}'