from functools import lru_cache

from pymongo import MongoClient

from scraper_server.db.config import get_db_settings

config = get_db_settings()


def _get_plot_from_collection_by_date(collection, date):
    date_to_match = date[:10]
    result = list(
        collection.find({"date_inserted": {"$regex": f"^{date_to_match}"}})
        .sort("_id", -1)
        .limit(1)
    )
    if result:
        del result[0]["_id"]
    else:
        result = None
    return result


class MongoAdapter:
    def __init__(self, database: str):
        if config.production:
            connection_par = (
                f"mongodb+srv://{config.mongo_db_user_name}:{config.mongo_db_password}@{config.mongo_db_host}"
                "/?retryWrites=true&w=majority"
            )
        else:
            connection_par = f"mongodb://{config.mongo_db_host}:{config.mongo_db_port}"
        self.cluster = MongoClient(connection_par)
        self.plot_collections = {
            "top10_plot": self.cluster[database]["top10_plot"],
            "spaghetti_plot": self.cluster[database]["spaghetti_plot"],
            "top_7_1_day_plot": self.cluster[database]["top_5_1_day_plot"],
            "top_20_1_month_plot": self.cluster[database]["top_20_1_month_plot"],
            "live_plot_data": self.cluster[database]["live_plot_data"]
        }

    def get_plot(self, name: str):
        if name in self.plot_collections:
            return list(self.plot_collections[name].find({}).sort("date_inserted", -1).limit(1))
        else:
            raise ValueError(f"No plot with name {name} exists in the database.")


@lru_cache(maxsize=1)
def get_mongo_adapter() -> MongoAdapter:
    return MongoAdapter("kormany_scraper_db")
