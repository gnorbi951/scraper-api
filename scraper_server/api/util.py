
def include_original(dec):
    def meta_decorator(f):
        decorated = dec(f)
        decorated._original = f
        return decorated
    return meta_decorator


@include_original
def remove_mongo_id(func):
    def wrapper(*args, **kwargs):
        response = func(*args, **kwargs)
        data = response[0]
        if "_id" in data:
            del data["_id"]
        return data
    return wrapper