from scraper_server.db.adapter import get_mongo_adapter

adapter = get_mongo_adapter()


class PlotCache:
    def __init__(self):
        self.plots = {
            'live_plot_data': [],
            'top_20_1_month_plot': [],
            'top_7_1_day_plot': [],
            'top10_plot': [],
            'spaghetti_plot': []
        }

    def get_plot(self, plot_name):
        if not self.plots[plot_name]:
            self.plots[plot_name] = adapter.get_plot(plot_name)
        return self.plots[plot_name]

    def refresh_plots(self):
        # refresh_minute = 5  # plot is refreshed in the 5th minute
        # iso_from, iso_to = 11, 13
        # hour = get_datetime_now().hour
        # minute = get_datetime_now().minute
        # # Don't refresh if it already has the latest data.
        # if minute > refresh_minute and int(self.get_plot('live_plot_data')[0].get('date_inserted')[iso_from:iso_to]) != hour:
        for plot_name in self.plots:
            plot_data = self.plots[plot_name]
            if not plot_data:
                continue
            self.plots[plot_name] = adapter.get_plot(plot_name)
