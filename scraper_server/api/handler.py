import json

from scraper_server.api.plot_cache import PlotCache
from scraper_server.api.util import remove_mongo_id
from scraper_server.models import ScrapingKeyWords, RateLimiter, get_rate_limiter

URL_PATH = "rawPath"
NOT_FOUND = {"status": 404, "description": "Not found"}
TOO_MANY_REQUESTS = {"status": 429, "description": "Too many requests"}
historic_data_rate_limiter = RateLimiter(limit=500, interval_sec=60 * 5)


def handle_requests(event, plot_cache: PlotCache):
    rate_limiter = get_rate_limiter()
    if rate_limiter() == "overload":
        return TOO_MANY_REQUESTS
    endpoint = event.get(URL_PATH)
    # if endpoint[:17] == "/scraper/keywords":
    #     return ScrapingKeyWords().get_frontend_topics()
    if endpoint[:14] == "/scraper/plots":
        return handle_scraper_requests(event, plot_cache)
    elif endpoint[:19] == "/scraper/keywordsv2":
        return sets_to_lists(ScrapingKeyWords().fields)
    elif endpoint[:5] == "/ping":
        return json.dumps({"success": True})
    else:
        return NOT_FOUND


@remove_mongo_id
def handle_scraper_requests(event, plot_cache: PlotCache):
    query_params = event.get("queryStringParameters")
    if query_params is None:
        return plot_cache.get_plot('spaghetti_plot')
    elif query_params.get('plot') == 'top_7_1_day_plot':
        return plot_cache.get_plot('top_7_1_day_plot')
    elif query_params.get('plot') == 'weekly_top_10':
        return plot_cache.get_plot('top10_plot')
    elif query_params.get('plot') == 'top_20_1_month_plot':
        return plot_cache.get_plot('top_20_1_month_plot')
    elif query_params.get('plot') == 'live':
        return plot_cache.get_plot('live_plot_data')
    else:
        return NOT_FOUND


def sets_to_lists(d):
    for key in d:
        d[key] = list(d[key])
    return d
