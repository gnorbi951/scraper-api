import pytz
from datetime import datetime


tz = pytz.timezone('Europe/Budapest')


def get_datetime_now() -> datetime:
    return datetime.now(tz=tz).replace(microsecond=0)


def get_iso_date_as_string() -> str:
    return datetime.now(tz=tz).isoformat()
