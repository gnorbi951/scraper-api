import logging
from scraper_server.api.handler import handle_requests
from scraper_server.api.plot_cache import PlotCache
from scraper_server.utils import get_datetime_now

logger = logging.getLogger(__name__)
plot_cache = PlotCache()


def lambda_handler(event, context):
    try:
        hour, minute = get_datetime_now().hour, get_datetime_now().minute
        iso_from, iso_to = 11, 13
        # Implement heartbeat
        if int(plot_cache.get_plot('live_plot_data')[0].get('date_inserted')[iso_from:iso_to]) != hour and minute > 5:
            plot_cache.refresh_plots()
        response = handle_requests(event, plot_cache)
    except Exception as e:
        logger.exception(f'Error: \n{e!r}')
        response = {"status": 500, "message": "Internal server error"}
    return response
